let __sedlex_table_4 =
  "\001\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\003"
  
let __sedlex_table_1 =
  "\001\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\003"
  
let __sedlex_table_3 =
  "\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\000\001\001\001"
  
let __sedlex_table_2 =
  "\001\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\003\002\002\002\004"
  
let __sedlex_partition_5 =
  function
  | Some uc ->
      let c = Uchar.to_int uc  in
      if c <= (-1)
      then (-1)
      else if c <= 40 then 0 else if c <= 41 then (-1) else 0
  | None  ->
      let c = (-1)  in
      if c <= (-1)
      then (-1)
      else if c <= 40 then 0 else if c <= 41 then (-1) else 0
  
let __sedlex_partition_1 =
  function
  | Some uc ->
      let c = Uchar.to_int uc  in
      if c <= 36 then (Char.code (__sedlex_table_1.[c - (-1)])) - 1 else 1
  | None  ->
      let c = (-1)  in
      if c <= 36 then (Char.code (__sedlex_table_1.[c - (-1)])) - 1 else 1
  
let __sedlex_partition_2 =
  function
  | Some uc ->
      let c = Uchar.to_int uc  in
      if c <= (-1)
      then (-1)
      else if c <= 35 then 0 else if c <= 36 then (-1) else 0
  | None  ->
      let c = (-1)  in
      if c <= (-1)
      then (-1)
      else if c <= 35 then 0 else if c <= 36 then (-1) else 0
  
let __sedlex_partition_3 =
  function
  | Some uc ->
      let c = Uchar.to_int uc  in
      if c <= 40 then (Char.code (__sedlex_table_2.[c - (-1)])) - 1 else 1
  | None  ->
      let c = (-1)  in
      if c <= 40 then (Char.code (__sedlex_table_2.[c - (-1)])) - 1 else 1
  
let __sedlex_partition_4 =
  function
  | Some uc ->
      let c = Uchar.to_int uc  in
      if c <= (-1)
      then (-1)
      else
        if c <= 39
        then (Char.code (__sedlex_table_3.[c])) - 1
        else if c <= 40 then (-1) else 0
  | None  ->
      let c = (-1)  in
      if c <= (-1)
      then (-1)
      else
        if c <= 39
        then (Char.code (__sedlex_table_3.[c])) - 1
        else if c <= 40 then (-1) else 0
  
let __sedlex_partition_6 =
  function
  | Some uc ->
      let c = Uchar.to_int uc  in
      if c <= 41 then (Char.code (__sedlex_table_4.[c - (-1)])) - 1 else 1
  | None  ->
      let c = (-1)  in
      if c <= 41 then (Char.code (__sedlex_table_4.[c - (-1)])) - 1 else 1
  
open Ast_mapper
open Ast_helper
open Asttypes
open Parsetree
open Longident
type part =
  | String of string 
  | Var of string 
exception Parse_error of string * int 
let escapeChar = '$'
  [@@ocaml.doc
    " Using \\\\ works but will produce warnings from the lexer. "]
let parse_string str =
  let parts = ref []  in
  let add item = parts := (item :: (!parts))  in
  let buf = Sedlexing.Utf8.from_string str  in
  let pos buf = let (start,_) = Sedlexing.loc buf  in start  in
  let rec loop () =
    let rec __sedlex_state_0 =
      function
      | buf ->
          (match __sedlex_partition_1 (Sedlexing.next buf) with
           | 0 -> 5
           | 1 -> __sedlex_state_2 buf
           | 2 -> __sedlex_state_3 buf
           | _ -> Sedlexing.backtrack buf)
    
    and __sedlex_state_2 =
      function
      | buf ->
          (Sedlexing.mark buf 0;
           (match __sedlex_partition_2 (Sedlexing.next buf) with
            | 0 -> __sedlex_state_2 buf
            | _ -> Sedlexing.backtrack buf))
    
    and __sedlex_state_3 =
      function
      | buf ->
          (match __sedlex_partition_3 (Sedlexing.next buf) with
           | 0 -> 3
           | 1 -> __sedlex_state_5 buf
           | 2 -> 1
           | 3 -> __sedlex_state_7 buf
           | _ -> Sedlexing.backtrack buf)
    
    and __sedlex_state_5 =
      function
      | buf ->
          (Sedlexing.mark buf 3;
           (match __sedlex_partition_4 (Sedlexing.next buf) with
            | 0 -> __sedlex_state_5 buf
            | _ -> Sedlexing.backtrack buf))
    
    and __sedlex_state_7 =
      function
      | buf ->
          (match __sedlex_partition_5 (Sedlexing.next buf) with
           | 0 -> __sedlex_state_8 buf
           | _ -> Sedlexing.backtrack buf)
    
    and __sedlex_state_8 =
      function
      | buf ->
          (match __sedlex_partition_6 (Sedlexing.next buf) with
           | 0 -> 4
           | 1 -> __sedlex_state_8 buf
           | 2 -> 2
           | _ -> Sedlexing.backtrack buf)
     in
    Sedlexing.start buf;
    (match __sedlex_state_0 buf with
     | 0 -> (add @@ (String (Sedlexing.Utf8.lexeme buf)); loop ())
     | 1 -> (add @@ (String (Sedlexing.Utf8.lexeme buf)); loop ())
     | 2 ->
         let token = Sedlexing.Utf8.lexeme buf  in
         let var = String.sub token 2 ((String.length token) - 3)  in
         (add @@ (Var var); loop ())
     | 3 ->
         raise
           (Parse_error
              ("Expected $ to be followed by '$' or '('", ((pos buf) + 1)))
     | 4 ->
         raise
           (Parse_error
              ("Expected closing ')' but found end of string",
                (String.length str)))
     | 5 -> ()
     | _ -> raise (Parse_error ("Unexpected character", (pos buf))))
     in
  loop (); List.rev (!parts) 
let to_str_code parts =
  let to_str =
    function
    | String str ->
        Exp.constant ~loc:(!Ast_helper.default_loc)
          (Pconst_string (str, None))
    | Var name ->
        Exp.ident @@
          { txt = (Longident.parse name); loc = (!Ast_helper.default_loc) }
     in
  {
    Ast_405.Parsetree.pexp_desc =
      (Ast_405.Parsetree.Pexp_apply
         ({
            Ast_405.Parsetree.pexp_desc =
              (Ast_405.Parsetree.Pexp_ident
                 {
                   Ast_405.Asttypes.txt =
                     (Ast_405.Longident.Ldot
                        ((Ast_405.Longident.Lident "String"), "concat"));
                   Ast_405.Asttypes.loc =
                     (Pervasives.(!) Ast_helper.default_loc)
                 });
            Ast_405.Parsetree.pexp_loc =
              (Pervasives.(!) Ast_helper.default_loc);
            Ast_405.Parsetree.pexp_attributes = []
          },
           [(Ast_405.Asttypes.Nolabel,
              {
                Ast_405.Parsetree.pexp_desc =
                  (Ast_405.Parsetree.Pexp_constant
                     (Ast_405.Parsetree.Pconst_string ("", None)));
                Ast_405.Parsetree.pexp_loc =
                  (Pervasives.(!) Ast_helper.default_loc);
                Ast_405.Parsetree.pexp_attributes = []
              });
           (Ast_405.Asttypes.Nolabel,
             (Ast_convenience.list (List.map to_str parts)))]));
    Ast_405.Parsetree.pexp_loc = (Pervasives.(!) Ast_helper.default_loc);
    Ast_405.Parsetree.pexp_attributes = []
  } 
let getenv_mapper argv =
  {
    default_mapper with
    expr =
      (fun mapper  ->
         fun expr  ->
           match expr with
           | { pexp_desc = Pexp_extension ({ txt = "str"; loc },pstr) } ->
               (match pstr with
                | PStr
                    ({
                       pstr_desc = Pstr_eval
                         ({ pexp_loc = loc;
                            pexp_desc = Pexp_constant (Pconst_string (sym,_))
                            },_)
                       }::[])
                    ->
                    (try
                       let parts = parse_string sym  in
                       Ast_helper.with_default_loc loc
                         (fun ()  -> to_str_code parts)
                     with
                     | Parse_error (message,pos) ->
                         let string_start =
                           (loc.Location.loc_start).Lexing.pos_cnum  in
                         let loc =
                           {
                             loc with
                             Location.loc_start =
                               {
                                 (loc.Location.loc_start) with
                                 Lexing.pos_cnum = ((string_start + 1) + pos)
                               };
                             loc_end =
                               {
                                 (loc.Location.loc_end) with
                                 Lexing.pos_cnum = ((string_start + 2) + pos)
                               }
                           }  in
                         let error =
                           Location.error ~loc ("Error: " ^ message)  in
                         raise (Location.Error error))
                | _ ->
                    raise
                      (Location.Error
                         (Location.error ~loc
                            "[%str] accepts a string, e.g. [%str \"USER\"]")))
           | x -> default_mapper.expr mapper x)
  } 
let () = register "str" getenv_mapper 
